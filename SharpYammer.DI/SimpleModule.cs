﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Ninject.Modules;

namespace SharpYammer.DI
{
    public class SimpleModule : NinjectModule
    {
        public override void Load()
        {
            //Bind<Colligo.Contracts.Events.ISearch>().To<Colligo.Events.Eventful.EventfulSearch>();
            Bind<Contracts.OAuth2.IOAuth2Client>().To<SharpYammer.OAuth2.Client>();
            Bind<Contracts.OAuth2.IUserInfo>().To<SharpYammer.OAuth2.UserInfo>();

        }
    }
}