﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpYammer.DI
{
    public interface IDIContainer
    {
        T Get<T>();
    }
}
