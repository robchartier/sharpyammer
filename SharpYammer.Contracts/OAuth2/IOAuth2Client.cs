﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace SharpYammer.Contracts.OAuth2
{
    public interface IOAuth2Client
    {
        string GetLoginLinkUri();
        IUserInfo Authenticate(NameValueCollection ResponseQueryString);

    }
}
