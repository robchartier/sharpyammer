﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpYammer.Contracts.OAuth2
{
    public interface IUserInfo
    {
        string Id { get; set; }
        string ProviderName { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhotoUri { get; set; }
    }
}
