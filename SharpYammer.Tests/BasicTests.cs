﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace SharpYammer.Tests
{
    [TestFixture]
    public class BasicTests
    {
        [Test]
        public void GetOAuthClient()
        {
            Contracts.OAuth2.IOAuth2Client client = DI.Container.Current.Get<Contracts.OAuth2.IOAuth2Client>();
            Assert.IsNotNull(client);
            Assert.IsNotNullOrEmpty(client.GetLoginLinkUri());
        }

        

    }
}