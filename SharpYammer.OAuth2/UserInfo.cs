﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpYammer.OAuth2
{
    public class UserInfo : Contracts.OAuth2.IUserInfo
    {
        public string Id { get; set; }

        public string ProviderName { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhotoUri { get; set; }
    }
}
