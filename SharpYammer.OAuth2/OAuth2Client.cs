﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OAuth2;
using System.Collections.Specialized;
using OAuth2.Client;
using SharpYammer.Contracts.OAuth2;

namespace SharpYammer.OAuth2
{
    public class Client : SharpYammer.Contracts.OAuth2.IOAuth2Client
    {
        private AuthorizationRoot _authorizationRoot;
        private IUserInfo _userInfo;
        public Client(AuthorizationRoot authorizationRoot, IUserInfo userInfo)
        {
            _authorizationRoot = authorizationRoot;
            this._userInfo = userInfo;
        }
        public string GetLoginLinkUri()
        {
            return _authorizationRoot.Clients.FirstOrDefault().GetLoginLinkUri();
        }
        public IUserInfo Authenticate(NameValueCollection ResponseQueryString)
        {
            var info = _authorizationRoot.Clients.FirstOrDefault().GetUserInfo(ResponseQueryString);
            this._userInfo.Email = info.Email;
            this._userInfo.FirstName = info.FirstName;
            this._userInfo.Id = info.Id;
            this._userInfo.LastName = info.LastName;
            this._userInfo.PhotoUri = info.PhotoUri;
            this._userInfo.ProviderName = info.ProviderName;
            return this._userInfo;
        }
    }
}
